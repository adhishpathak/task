FROM python:3.8.2-alpine
MAINTAINER adhish pathak 
ADD . /task
WORKDIR /task 
# You will need this if you need PostgreSQL, otherwise just skip this
RUN apk update && apk add gcc python3-dev musl-dev libffi-dev
RUN pip install -r requirements.txt
EXPOSE 8000
CMD ["./manage.py", "runserver"]