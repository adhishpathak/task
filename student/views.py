from .models import Course, Student
from .serializers import StudentSerializer, CourseSerializer
from rest_framework import generics


class StudentList(generics.ListCreateAPIView):
    """
    List all students, or create a new student.
    """

    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a student instance.
    """

    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class CourseList(generics.ListCreateAPIView):
    """
    List all courses, or create a new course.
    """

    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class CourseDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a course instance.
    """

    queryset = Course.objects.all()
    serializer_class = CourseSerializer