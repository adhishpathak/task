from django.db import models
from django.db.models import Count
from django.db.models.signals import m2m_changed


class Course(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField(max_length=100, blank=False)
    address = models.CharField("Address", max_length=1024,)
    course = models.ManyToManyField('Course', related_name='students', blank=True)
    join_date = models.DateTimeField(auto_now_add=True)
    courses_count = models.IntegerField(blank=True, default=0)

    class Meta:
        ordering = ['join_date']

    def __str__(self):
        return self.name


def courses_changed(sender, instance, action, **kwargs):
    
    instance.courses_count = instance.course.count()
    instance.save()


m2m_changed.connect(courses_changed, sender=Student.course.through)
