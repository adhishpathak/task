from rest_framework import serializers
from .models import Course, Student
from django.db.models import Count
        

class CourseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Course
        fields = ['id', 'name']


class StudentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Student
        fields = ['id', 'name', 'address', 'course', 'courses_count', 'join_date']
        read_only_fields = ['courses_count']
