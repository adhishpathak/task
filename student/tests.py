from rest_framework import status
from rest_framework.test import APITestCase
import json


# Create your tests here.
class StudentTest(APITestCase):
    """ Test module for GET all students API """
   
    def test_create_student(self):
        data = data = { "name": "Adhish", "address": "12 b loknayak nagar indore", "course": [1, 2, 3] }
        response = self.client.post("/students", json.dumps(data), format='json', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_update_student(self):
        data = data = { "name": "Adhish", "address": "12 b loknayak nagar indore", "course": [1, 2, 3] }
        response = self.client.put("/students", json.dumps(data), format='json', follow=True)
        self.assertEqual(response.status_code, 200)
